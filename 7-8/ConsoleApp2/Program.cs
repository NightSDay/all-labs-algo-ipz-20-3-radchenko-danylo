﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        public static void BFS(int f)           //алгоритм BFS в ширину
        {
            string start = Routes[f];
            Marked[f] = true;
            Queue.Enqueue(f);
            Distance.Add(0);
            while (Queue.Count != 0)
            {
                f = Queue.Dequeue();
                for (int i = 0; i < n; i++)
                {
                    if (MatrizaSmezhnosti[f, i] != 0 && !Marked[i])
                    {
                        Marked[i] = true;
                        Queue.Enqueue(i);
                        Distance.Add(Distance[f] + MatrizaSmezhnosti[f, i]);
                        Console.WriteLine(start + " - " + Routes[i] + ": " + Distance[i]);
                    }
                }
            }

            static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
