﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        struct num
        {
            public short n0;
            public short n1;
            public short n2;
            public short n3;
            public short n4;
            public short n5;
            public short n6;
            public short n7;
            public short n8;
            public short n9;
            public short n10;
            public short n11;
            public short n12;
            public short n13;
            public short n14;
            public short n15;
        }
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            num nums;
            nums.n0 = 1;
            nums.n1 = 1;
            nums.n2 = 1;
            nums.n3 = 1;
            nums.n4 = 1;
            nums.n5 = 1;
            nums.n6 = 1;
            nums.n7 = 1;
            nums.n8 = 1;
            nums.n9 = 1;
            nums.n10 = 1;
            nums.n11 = 1;
            nums.n12 = 1;
            nums.n13 = 1;
            nums.n14 = 1;
            nums.n15 = 1;
            Int16 n15;
            Console.WriteLine("Введіть число: "); n15 = Int16.Parse(Console.ReadLine());
            if (n15 > 0)
            {
                Console.WriteLine("Знак : +");
            }
            else if (n15 < 0)
            {
                Console.WriteLine("Знак : -");
            }
            else
            {
                Console.WriteLine("Число нуль");
            }
        }
    }
}
