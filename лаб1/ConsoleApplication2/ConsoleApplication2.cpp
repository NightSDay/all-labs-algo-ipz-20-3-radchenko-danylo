﻿#include <iostream>
#include <Windows.h>    

union global {
    union num {
        struct n {
            unsigned short a0 : 1;
            unsigned short a1 : 1;
            unsigned short a2 : 1;
            unsigned short a3 : 1;
            unsigned short a4 : 1;
            unsigned short a5 : 1;
            unsigned short a6 : 1;
            unsigned short a7 : 1;
            unsigned short a8 : 1;
            unsigned short a9 : 1;
            unsigned short a10 : 1;
            unsigned short a11 : 1;
            unsigned short a12 : 1;
            unsigned short a13 : 1;
            unsigned short a14 : 1;
            unsigned short a15 : 1;
        }numer;
        signed short count;
    }num1;
    union numbers {
        struct number {
            unsigned short b0 : 1;
            unsigned short b1 : 1;
            unsigned short b2 : 1;
            unsigned short b3 : 1;
            unsigned short b4 : 1;
            unsigned short b5 : 1;
            unsigned short b6 : 1;
            unsigned short b7 : 1;
            unsigned short b8 : 1;
            unsigned short b9 : 1;
            unsigned short b10 : 1;
            unsigned short b11 : 1;
            unsigned short b12 : 1;
            unsigned short b13 : 1;
            unsigned short b14 : 1;
            unsigned short b15 : 1;
            unsigned short b16 : 1;
            unsigned short b17 : 1;
            unsigned short b18 : 1;
            unsigned short b19 : 1;
            unsigned short b20 : 1;
            unsigned short b21 : 1;
            unsigned short b22 : 1;
            unsigned short b23 : 1;
            unsigned short b24 : 1;
            unsigned short b25 : 1;
            unsigned short b26 : 1;
            unsigned short b27 : 1;
            unsigned short b28 : 1;
            unsigned short b29 : 1;
            unsigned short b30 : 1;
            unsigned short b31 : 1;
        }number;
        struct num3 {
            unsigned short val1 : 8;
            unsigned short val2 : 8;
            unsigned short val3 : 8;
            unsigned short val4 : 8;
        }num4;
        float val;
    }num2;

}global;
int main()
{
    system("chcp 1251");
    system("cls");
    float a;
    printf("Input element: ");
    scanf_s("%f", &global.num2.val);
    printf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", global.num2.number.b31, global.num2.number.b30, global.num2.number.b29, global.num2.number.b28, global.num2.number.b27, global.num2.number.b26, global.num2.number.b25, global.num2.number.b24, global.num2.number.b23, global.num2.number.b22, global.num2.number.b21,
        global.num2.number.b20, global.num2.number.b19, global.num2.number.b18, global.num2.number.b17, global.num2.number.b16, global.num2.number.b15, global.num2.number.b14, global.num2.number.b13, global.num2.number.b12, global.num2.number.b11,
        global.num2.number.b10, global.num2.number.b9, global.num2.number.b8, global.num2.number.b7, global.num2.number.b6, global.num2.number.b5, global.num2.number.b4, global.num2.number.b3, global.num2.number.b2, global.num2.number.b1, global.num2.number.b0);
    //
    printf("\n");
    printf("\n");
    //
    printf("%d %d %d %d", global.num2.num4.val1, global.num2.num4.val2,
        global.num2.num4.val3, global.num2.num4.val4);
    //
    printf("\n");
    printf("\n");
    //
    printf("Мантиса: %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n\n", global.num2.number.b22, global.num2.number.b21,
        global.num2.number.b20, global.num2.number.b19, global.num2.number.b18, global.num2.number.b17, global.num2.number.b16, global.num2.number.b15, global.num2.number.b14, global.num2.number.b13, global.num2.number.b12, global.num2.number.b11,
        global.num2.number.b10, global.num2.number.b9, global.num2.number.b8, global.num2.number.b7, global.num2.number.b6, global.num2.number.b5, global.num2.number.b4, global.num2.number.b3, global.num2.number.b2, global.num2.number.b1, global.num2.number.b0);
    printf("Знак числа: %d\n", global.num2.number.b31);
    printf("Степінь: %d%d%d%d%d%d%d%d", global.num2.number.b30, global.num2.number.b29,
        global.num2.number.b28, global.num2.number.b27, global.num2.number.b26,
        global.num2.number.b25, global.num2.number.b24, global.num2.number.b23);
    printf("\nСтруктура займає в пам'яті: %d байт", sizeof(global));
    return 0;
}
