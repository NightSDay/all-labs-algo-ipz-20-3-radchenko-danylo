﻿#include <iostream>
#include <Windows.h>    

union global {
    union num {
        struct n {
            unsigned short a0 : 1;
            unsigned short a1 : 1;
            unsigned short a2 : 1;
            unsigned short a3 : 1;
            unsigned short a4 : 1;
            unsigned short a5 : 1;
            unsigned short a6 : 1;
            unsigned short a7 : 1;
            unsigned short a8 : 1;
            unsigned short a9 : 1;
            unsigned short a10 : 1;
            unsigned short a11 : 1;
            unsigned short a12 : 1;
            unsigned short a13 : 1;
            unsigned short a14 : 1;
            unsigned short a15 : 1;
        }numer;
        signed short count;
    }num1;
    union numbers {
        struct number {
            unsigned short b0 : 1;
            unsigned short b1 : 1;
            unsigned short b2 : 1;
            unsigned short b3 : 1;
            unsigned short b4 : 1;
            unsigned short b5 : 1;
            unsigned short b6 : 1;
            unsigned short b7 : 1;
            unsigned short b8 : 1;
            unsigned short b9 : 1;
            unsigned short b10 : 1;
            unsigned short b11 : 1;
            unsigned short b12 : 1;
            unsigned short b13 : 1;
            unsigned short b14 : 1;
            unsigned short b15 : 1;
            unsigned short b16 : 1;
            unsigned short b17 : 1;
            unsigned short b18 : 1;
            unsigned short b19 : 1;
            unsigned short b20 : 1;
            unsigned short b21 : 1;
            unsigned short b22 : 1;
            unsigned short b23 : 1;
            unsigned short b24 : 1;
            unsigned short b25 : 1;
            unsigned short b26 : 1;
            unsigned short b27 : 1;
            unsigned short b28 : 1;
            unsigned short b29 : 1;
            unsigned short b30 : 1;
            unsigned short b31 : 1;
        }number;
        struct num3 {
            unsigned short val1 : 8;
            unsigned short val2 : 8;
            unsigned short val3 : 8;
            unsigned short val4 : 8;
        }num4;
        float val;
    }num2;
}global;

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    do {
        printf("Input element of the arrange[-32768;32767] : ");
        scanf_s("%f", &global.num2.val);
    } while (global.num2.val > 32767 || global.num2.val < -32768);
    int x = global.num2.val;
    //
    printf("\n");
    //
    printf("number is = %f", global.num2.val);
    //
    printf("\n");
    //
    printf("\nFirst\n");
    if (global.num2.number.b15 == 0)
    {
        printf("число додатнє,знак +.Біт перший  %d\n", global.num2.number.b16);
    }
    else {
        printf("число від'ємне,знак -.Біт перший  %d\n", global.num2.number.b16);
    }
    printf("\nSecond\n");
    int inversionNum = ~x + 1;
    printf("\n ~num +1 = %d\n", inversionNum);
    if (x > inversionNum)
    {
        printf("число від'ємне,знак -.Біт перший  %d\n", global.num2.number.b16);
    }
    else {
        printf("число додатнє,знак +.Біт перший  %d\n", global.num2.number.b16);
    }
    return 0;
}
