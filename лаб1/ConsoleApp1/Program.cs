﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        struct Date
        {
            public int MonthDay;
            public int Month;
            public int Year;
            public int WeekDay;
            public int Hours;
            public int Minutes;
            public int Seconds;
        }
        public static void Main()
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;

            DateTime localDate = DateTime.Now;
            String[] cultureNames = { "ua-UA" };

            foreach (var cultureName in cultureNames)
            {
                var culture = new CultureInfo(cultureName);
                Console.WriteLine("{0}:", culture.NativeName);
                Console.WriteLine("   Час на поточній машині: {0}, {1:G}",
                                  localDate.ToString(culture), localDate.Kind);
            }
            Date d;
            d.MonthDay = 6;
            d.WeekDay = 3;
            d.Month = 5;
            d.Year = 8;
            d.Hours = 5;
            d.Minutes = 6;
            d.Seconds = 6;

            int p = d.Year + d.WeekDay + d.MonthDay + d.Month + d.Hours + d.Minutes + d.Seconds;
            Console.WriteLine("\nРозмір структури = {0}", p);
            Console.ReadKey();
        }
    }
}
