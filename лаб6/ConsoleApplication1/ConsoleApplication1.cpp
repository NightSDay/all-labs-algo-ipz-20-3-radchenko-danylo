﻿#include <iostream>
#include <stdio.h>
#include "windows.h"
#include "stdlib.h"
#include "time.h"
#include <ctime>
#include <cstdlib>
#include "math.h"
#define MAX_SIZE 10
#include <chrono>
#include <random>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>
#define left_child(node) ( (node) * 2 + 1 ) 
#define right_child(node) ( (node) * 2 + 2 ) 

void swap(int* array, int i, int j)
{
    int tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
}

void heap_it(int* array, int length, int root)
{
    int leftChild = left_child(root);
    int rightChild = right_child(root);
    int biggest = root;

    if (leftChild < length && array[root] < array[leftChild])
        biggest = leftChild;
    if (rightChild < length && array[biggest] < array[rightChild])
        biggest = rightChild;
    if (biggest != root)
    {
        swap(array, biggest, root);
        heap_it(array, length, biggest);
    }
}

void make_heap(int* array, int length)
{
    int i = length / 2;

    for (; i >= 0; --i)
        heap_it(array, length, i);
}

void heap_sort(int* array, int count)
{
    int last;

    make_heap(array, count);
    for (last = count - 1; last > 0; --last)
    {
        swap(array, 0, last);
        heap_it(array, last, 0);
    }
}

#define COUNT (10)


int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    auto begin = GETTIME();


    srand(time(0));
    int i;
    int array[COUNT];
    for (int f = 0; f < COUNT; f++)
    {
        array[f] = 0 + rand() % (100 + 1);

    }

    printf("unSorted:\n");
    for (i = 0; i < COUNT; ++i)
        printf("%d ", array[i]);

    heap_sort(array, COUNT);
    printf("\nSorted:\n");

    for (i = 0; i < COUNT; ++i)
        printf("%d ", array[i]);
    printf("\n");
    auto end = GETTIME();
    auto elapsed_ns = CALCTIME(end - begin);
    printf("The time: %lld ns\n", elapsed_ns.count());

}
