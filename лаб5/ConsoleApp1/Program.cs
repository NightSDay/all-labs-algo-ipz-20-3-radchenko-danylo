﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace ggggg
{
    class Program
    {
        static void SortSelectionLinkedList(LinkedList<int> list) // Сортування вибором двозв'язного списку
        {
            for (LinkedListNode<int> iNode = list.First; iNode != null; iNode = iNode.Next)
            {
                LinkedListNode<int> minNode = iNode;
                for (LinkedListNode<int> jNode = iNode.Next; jNode != null; jNode = jNode.Next)
                    if (jNode.Value < minNode.Value)
                        minNode = jNode;

                // Перестановка значень
                int temp = iNode.Value;
                iNode.Value = minNode.Value;
                minNode.Value = temp;
            }
        }

        static void SortInsertionLinkedList(LinkedList<int> list) // Сортування двозв'язного списку вставками
        {
            LinkedListNode<int> node = list.First;
            for (LinkedListNode<int> iNode = node.Next; iNode != null; iNode = iNode.Next)
            {
                int temp = iNode.Value;
                for (LinkedListNode<int> jNode = iNode.Previous; jNode != null && jNode.Value > temp; jNode = jNode.Previous)
                {
                    jNode.Next.Value = jNode.Value;
                    jNode.Value = temp;
                }
            }
        }

        static void Main(string[] args)
        {
            LinkedList<int> list1 = new LinkedList<int>();
            LinkedList<int> list2 = new LinkedList<int>();

            Random rnd = new Random();

            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

            stopwatch.Start();

            int size = 100;

            for (int i = 0; i < size; i++)
            {
                int random = rnd.Next(200);
                list1.AddLast(random);
                list2.AddLast(random);
            }

            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }

            Console.WriteLine("\n\n\n");

            SortSelectionLinkedList(list1);

            foreach (var item in list1)
            {
                Console.Write("\t" + item);
            }

            stopwatch.Stop();
            Console.WriteLine("\n");
            Console.WriteLine("Time: " + stopwatch.Elapsed);

        }
    }
}
